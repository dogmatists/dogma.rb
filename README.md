# Dogma for Ruby

[![Project license](https://img.shields.io/badge/license-Public%20Domain-blue.svg)](https://unlicense.org)
[![RubyGems gem version](https://img.shields.io/gem/v/dogma.rb.svg)](https://rubygems.org/gems/dogma.rb)
